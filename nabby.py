import sanic
import youtube_dl
import streamlink
import os
import json
import re
import requests
from config import debug, state_file, ytdl_format, sanic_host, sanic_port,\
    ytdl_max_filesize, ytdl_filename, cache_folder, max_cache_size, main_text,\
    instance_url

app = sanic.Sanic()


video_files = {}
video_states = {}

ytid_regex = re.compile(r"[a-zA-Z0-9_-]{11}")


def get_yt_video(video_id):
    print(f"Downloading {video_id}...")
    video_url = f"https://youtu.be/{video_id}"
    with youtube_dl.YoutubeDL(ydl_opts) as ydl:
        ydl.download([video_url])


def ytdl_hook(d):
    # If debug mode is enabled, show ytdl status
    if debug:
        print(repr(d))

    # Extract video id from result filename. Hacky.
    video_id = d["filename"].split("/")[-1].split(".")[0]

    # Save the state that'll be displayed to the user when file is downloading
    if d["status"] == "finished":
        del video_states[video_id]
        # HACK: if it's a single file, then set it as video_file
        check_if_vid_exists(video_id)
        return

    # Don't update state if speed strings are not existent
    if "_speed_str" not in d:
        return

    video_states[video_id] = f"Downloading {d['_total_bytes_str']} at "\
                             f"{d['_speed_str']}, {d['_percent_str']} done, "\
                             f"ETA {d['_eta_str']}."


ydl_opts = {
    "quiet": True,
    "restrictfilenames": True,
    "noplaylist": True,
    "nooverwrites": True,
    "merge_output_format": "webm",
    "format": ytdl_format,
    "max_filesize": ytdl_max_filesize,
    "outtmpl": ytdl_filename,
    "progress_hooks": [ytdl_hook],
}


@app.route("/")
async def main(request):
    return sanic.response.html(main_text)


def check_if_vid_exists(video_id):
    vid_path = os.path.join(cache_folder, f"{video_id}.webm")
    if os.path.exists(vid_path):
        video_files[video_id] = vid_path
        print(f"Downloaded {video_id}...")
        flush_cache(video_id)
        save_state()


# the whole twitch handling is really hecky
# but it works as expected so I'll say "fuck it"
async def twitch_stream_url(request, url, chunk_size=8192):
    loop = request.app.loop
    streams = await loop.run_in_executor(None, streamlink.streams, url)
    print(f"Opening stream...")
    fd = streams["best"].open()

    async def streaming_fn(response):
        try:
            while(True):
                data = await loop.run_in_executor(None, fd.read, chunk_size)
                if len(data) < 1:
                    break
                await response.write(data)
        finally:
            fd.close()
        return
    return sanic.response.stream(streaming_fn, content_type='video/unknown')


@app.route("/twitch/stream/<channel>")
async def twitch_stream(request, channel):
    print(f"Received request for Twitch Stream {channel}")
    return await twitch_stream_url(request, f"https://twitch.tv/{channel}")


@app.route("/twitch/vod/<vidid>")
async def twitch_vod(request, vidid):
    print(f"Received request for Twitch VoD {vidid}")
    return await twitch_stream_url(request, f"https://twitch.tv/videos/{vidid}")


@app.route("/rss/youtube/<channel_id>")
async def rss(request, channel_id):
    contents = requests.get("https://www.youtube.com/feeds/videos.xml"
                            f"?channel_id={channel_id}")
    contents = contents.text
    contents = contents.replace("https://www.youtube.com/watch",
                                instance_url + "/watch")
    return sanic.response.text(contents, content_type="application/xml")


@app.route("/<video_id>")
async def yt_watch(request, video_id):
    # This is a hack, but I couldn't manage to get it working otherwise, oof.
    if video_id == "watch":
        video_id = request.args.get("v")

    # HACK: Handled combined videos case as
    # we're not notified about them by youtube-dl
    check_if_vid_exists(video_id)

    if not ytid_regex.fullmatch(video_id):
        return sanic.response.html("This isn't a valid YouTube video ID.")

    # If video is present, return that to the user.
    if video_id in video_files:
        return await sanic.response.file_stream(video_files[video_id])
    # If video is currently being downloaded
    # inform user about the state and refresh page every 5 secs
    elif video_id in video_states:
        return sanic.response.html("Video is currently downloading.<br>"
                                   "Last state: " +
                                   video_states[video_id],
                                   headers={"Refresh": "5"})
    else:
        # Prevent video downloads from being started more than once.
        video_states[video_id] = "Download started.<br>"\
                                 "If state doesn't change for a while, "\
                                 "your download may not be possible "\
                                 "(region lock, premium video, wrong URL, "\
                                 "deleted video, too big etc)."

        # Start video download
        await request.app.loop.run_in_executor(None,
                                               get_yt_video,
                                               video_id)

        # Inform user that download started, refresh page every 5 secs
        return sanic.response.html("Video download started.",
                                   headers={"Refresh": "5"})


def save_state():
    global video_files
    with open(state_file, "w") as f:
        json.dump(video_files, f)


def load_state():
    global video_files
    if not os.path.exists(state_file):
        with open(state_file, "w") as f:
            f.write("{}")

    with open(state_file, "r") as f:
        video_files = json.load(f)


def delete_part_files():
    cache_dir = os.listdir(cache_folder)

    for fname in cache_dir:
        if fname.endswith(".part"):
            os.remove(os.path.join(cache_folder, fname))


def flush_cache(ignored=""):
    cache_dir = os.listdir(cache_folder)

    total_size = 0
    file_sizes = {}

    for fname in cache_dir:
        full_fname = os.path.join(cache_folder, fname)
        fsize = os.path.getsize(full_fname)
        file_sizes[full_fname] = fsize
        total_size += fsize

    # If cache size isn't bigger than max allowed size, don't take any actions
    if total_size < max_cache_size:
        return

    # Sort by filesize
    file_sizes = sorted(file_sizes.items(),
                        reverse=True,
                        key=lambda kv: kv[1])

    checking = 0

    # Delete files while we're still above limit
    while total_size > max_cache_size:
        fname, fsize = file_sizes[checking]
        video_id = fname.split(".")[0].split("/")[-1]

        # Don't delete the newly downloaded video
        # even if it is the biggest
        if ignored and video_id == ignored:
            checking += 1
            continue

        os.remove(fname)
        total_size -= fsize
        if video_id in video_files:
            del video_files[video_id]
        del file_sizes[checking]
    save_state()


if __name__ == '__main__':
    delete_part_files()
    load_state()
    flush_cache()
    app.run(host=sanic_host, port=sanic_port)
